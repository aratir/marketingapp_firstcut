package voltas.iotalabs.marketingacapp_android;



import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;



/**
 * A simple {@link Fragment} subclass.
 */
public class CompareFragment extends Fragment {
    private  int intSetTemp;
    private  int intRoomTemp;
    private ImageView imgConv;
    private ImageView imgInv;
    private SeekBar seekBar1;
    private SeekBar seekBar2;
    private ImageView btnPlay;
    private TextView txtCurrTemp1, txtCurrTemp2;
    private int TempDiff=0;
    private View rootView;
    private final Handler handler = new Handler();
    AnimationDrawable  InvAnimation, ConvAnimation;
    RangeBar tempSeekBarConv, tempSeekbarInv;
    private boolean booleanCancelMember=true, boolFirstTime=true;


    public CompareFragment() {
        // Required empty public constructor
    }

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment\
       try {
           System.out.println("Entered compare frag1");
           rootView = inflater.inflate(R.layout.fragment_compare, container, false);

           imgConv = (ImageView) rootView.findViewById(R.id.imageConv);
           imgInv = (ImageView) rootView.findViewById(R.id.imageInv);

           imgConv.setBackgroundResource(R.drawable.conventional_normal_1);
           imgInv.setBackgroundResource(R.drawable.inverter_fast_1);

           seekBar1 = (SeekBar) rootView.findViewById(R.id.seekBar1);
           seekBar2 = (SeekBar) rootView.findViewById(R.id.seekBar2);

           txtCurrTemp1 = (TextView) rootView.findViewById(R.id.textViewTemp1);
           txtCurrTemp2 = (TextView) rootView.findViewById(R.id.textViewTemp2);

           seekBar1.setProgress(0);
           seekBar2.setProgress(0);

           intRoomTemp = TemperatureSettings.GetRoomTemp();
           intSetTemp = TemperatureSettings.GetSetTemp();

           tempSeekBarConv = (RangeBar) rootView.findViewById(R.id.rsbConv);
           tempSeekbarInv= (RangeBar) rootView.findViewById(R.id.rsbInv);

           btnPlay = (ImageView) rootView.findViewById(R.id.btnPlay);
           System.out.println("before play");
           if (intSetTemp != 0 && intRoomTemp !=0) {
               if (btnPlay != null)

                   btnPlay.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           System.out.println("Inside Play listener");
                           btnPlay.setImageResource(R.drawable.pause2);
                           System.out.println("Inside timer");
                           TempDiff = (intRoomTemp - intSetTemp);
                           handler.postDelayed(runnable, 1000);
                       }
                   });

           }
           else
               Toast.makeText(getActivity(), "Please set the temperatures in Settings Screen", Toast.LENGTH_SHORT).show();
       }
       catch (Exception e) {

        e.printStackTrace();
       }
       return rootView;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(booleanCancelMember != false) {

                ComparisonCalc();
                handler.postDelayed(this, 10000);
            }
        }
    };


     public void ComparisonCalc() {
        //Calculations and image changes go here
        try {
            if (InvAnimation !=null && ConvAnimation !=null) {
                InvAnimation.stop();
                ConvAnimation.stop();
            }
            System.out.println("Temp diff " + TempDiff);
            if (TempDiff >= 3) {
                if (imgConv != null && imgInv != null) {
                    imgInv.setBackgroundResource(R.drawable.inverter_fast);
                    InvAnimation = (AnimationDrawable) imgInv.getBackground();

                    imgConv.setBackgroundResource(R.drawable.conventional_normal);
                    ConvAnimation = (AnimationDrawable) imgConv.getBackground();

                    InvAnimation.start();
                    ConvAnimation.start();
                 }
                seekBar1.setProgress(seekBar1.getMax());
                seekBar2.setProgress(seekBar2.getMax());

                txtCurrTemp1.setText(getResources().getString(R.string.Temp)+ intRoomTemp + " C");
                txtCurrTemp2.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");


            } else if (TempDiff < 3 && TempDiff >= 1) {
                if (imgConv != null && imgInv != null) {
                    imgInv.setBackgroundResource(R.drawable.inverter_medium);
                    InvAnimation = (AnimationDrawable) imgInv.getBackground();

                    imgConv.setBackgroundResource(R.drawable.conventional_normal);
                    ConvAnimation = (AnimationDrawable) imgConv.getBackground();

                    txtCurrTemp1.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");
                    txtCurrTemp2.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");

                    InvAnimation.start();
                    ConvAnimation.start();
                }
                seekBar1.setProgress(seekBar1.getMax());
                seekBar2.setProgress(seekBar2.getMax() / 2);

            } else if (TempDiff ==0) {
                if (imgConv != null && imgInv != null) {
                    imgInv.setBackgroundResource(R.drawable.inverter_slow);
                    InvAnimation = (AnimationDrawable) imgInv.getBackground();

                    imgConv.setBackgroundResource(R.drawable.conventional_fanonly);
                    ConvAnimation = (AnimationDrawable) imgConv.getBackground();

                    txtCurrTemp1.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");
                    txtCurrTemp2.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");

                    InvAnimation.start();
                    ConvAnimation.start();

                    btnPlay.setImageResource(R.drawable.play_4);

                }

                seekBar1.setProgress(seekBar1.getMax());
                seekBar2.setProgress(seekBar2.getMax() / 4);
            } else if (TempDiff <= -1) {
                if (imgConv != null && imgInv != null) {
                    imgInv.setBackgroundResource(R.drawable.inverter_fanonly);
                    InvAnimation = (AnimationDrawable) imgInv.getBackground();

                    imgConv.setBackgroundResource(R.drawable.conventional_fanonly);
                    ConvAnimation = (AnimationDrawable) imgConv.getBackground();

                    txtCurrTemp1.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");
                    txtCurrTemp2.setText(getResources().getString(R.string.Temp) + intRoomTemp + " C");

                    InvAnimation.start();
                    ConvAnimation.start();

                    seekBar1.setProgress(0);
                    seekBar2.setProgress(0);

                    btnPlay.setImageResource(R.drawable.play_4);

                }
            }

            if (intRoomTemp > intSetTemp) {

                tempSeekBarConv.setRangePinsByValue(intSetTemp, intRoomTemp);
                tempSeekbarInv.setRangePinsByValue(intSetTemp, intRoomTemp);

            } else if (intRoomTemp < intSetTemp) {
                tempSeekBarConv.setRangePinsByValue(intRoomTemp, intSetTemp);
                tempSeekbarInv.setRangePinsByValue(intRoomTemp, intSetTemp);

            } else {
                tempSeekBarConv.setRangePinsByValue(intRoomTemp, intSetTemp);
                tempSeekbarInv.setRangePinsByValue(intRoomTemp, intSetTemp);

                booleanCancelMember = false;
            }

            System.out.println("Temp " + intRoomTemp);
            if (TempDiff > 0)
                TempDiff = TempDiff - 1;
            else
                TempDiff = TempDiff + 1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
         finally {

            handler.removeCallbacks(runnable);
            System.out.println("Timer off");
            boolFirstTime=false;
            if (intRoomTemp > intSetTemp)
                intRoomTemp = intRoomTemp - 1;
            else if (intRoomTemp < intSetTemp)
                intRoomTemp = intRoomTemp + 1;

        }
    }

}


