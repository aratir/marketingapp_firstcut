package voltas.iotalabs.marketingacapp_android;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private View rootView;
    private EditText txtOutsideTemp, txtSetTemp, txtRoomTemp;
    private int intOutsideTemp, intSetTemp, intRoomTemp;
    private Button btnSave;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("Entered settings frag1");
        System.out.println("Settings frag");
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        System.out.println("After rootview in Settings frag");

        txtOutsideTemp = (EditText) rootView.findViewById(R.id.editViewOutside);
        txtSetTemp = (EditText) rootView.findViewById(R.id.editViewSet);
        txtRoomTemp = (EditText) rootView.findViewById(R.id.editViewRoom);


        System.out.println(txtOutsideTemp.getText().toString() + ":" + txtSetTemp.getText().toString() + ":" + txtRoomTemp.getText().toString());

        btnSave = (Button) rootView.findViewById(R.id.btnSave);

        if (btnSave != null)
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtOutsideTemp = (EditText) rootView.findViewById(R.id.editViewOutside);
                    txtSetTemp = (EditText) rootView.findViewById(R.id.editViewSet);
                    txtRoomTemp = (EditText) rootView.findViewById(R.id.editViewRoom);

                    if (txtSetTemp != null)
                        intSetTemp = Integer.parseInt(txtSetTemp.getText().toString());
                    if (txtOutsideTemp != null)
                        intOutsideTemp = Integer.parseInt(txtOutsideTemp.getText().toString());
                    if (txtRoomTemp != null)
                        intRoomTemp = Integer.parseInt(txtRoomTemp.getText().toString());

                    if (intSetTemp<15 || intSetTemp >35)
                    {
                        Toast.makeText(getActivity(), "Set Temperature has to be between 15 and 35 degree C", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(intRoomTemp <15 || intRoomTemp>45)
                    {
                        Toast.makeText(getActivity(), "Room Temperature has to be between 15 and 45 degree C", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    TemperatureSettings.SetTemp = Integer.parseInt(txtSetTemp.getText().toString());
                    TemperatureSettings.OutsideTemp = Integer.parseInt(txtOutsideTemp.getText().toString());
                    TemperatureSettings.RoomTemp = Integer.parseInt(txtRoomTemp.getText().toString());

                    Toast.makeText(getActivity(), "Temperature Settings saved", Toast.LENGTH_SHORT).show();
                }
            });
        return rootView;
    }
}
