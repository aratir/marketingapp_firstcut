package voltas.iotalabs.marketingacapp_android;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private CompareFragment compareFragment = null;
    private SettingsFragment settingsFragment = null;
    private ValueCreation valueFragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((ImageView)findViewById(R.id.imgCompare)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (compareFragment == null) {
                    compareFragment = new CompareFragment();
                }

                findViewById(R.id.imgCompare).setBackgroundResource(R.drawable.sidebar_selected);
                findViewById(R.id.imgmoneybag).setBackgroundResource(0);
                findViewById(R.id.imgsetting).setBackgroundResource(0);

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, compareFragment)
                        .commit();
            }
        });

        ((ImageView)findViewById(R.id.imgsetting)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if(settingsFragment == null){
                    settingsFragment = new SettingsFragment();
                }

                findViewById(R.id.imgsetting).setBackgroundResource(R.drawable.sidebar_selected);
                findViewById(R.id.imgCompare).setBackgroundResource(0);
                findViewById(R.id.imgmoneybag).setBackgroundResource(0);

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, settingsFragment)
                        .commit();
            }
        });

        ((ImageView)findViewById(R.id.imgmoneybag)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if(valueFragment == null){
                    valueFragment = new ValueCreation();
                }

                findViewById(R.id.imgmoneybag).setBackgroundResource(R.drawable.sidebar_selected);
                findViewById(R.id.imgCompare).setBackgroundResource(0);
                findViewById(R.id.imgsetting).setBackgroundResource(0);

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, valueFragment)
                        .commit();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
