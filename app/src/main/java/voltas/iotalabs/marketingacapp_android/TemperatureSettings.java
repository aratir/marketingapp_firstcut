package voltas.iotalabs.marketingacapp_android;

/**
 * Created by Arati on 18-01-2016.
 */
public class TemperatureSettings {
    public static int RoomTemp;
    public static int OutsideTemp;
    public static int SetTemp;

    public static int GetRoomTemp() {
        return RoomTemp;
    }

    public static int GetSetTemp() {
        return SetTemp;
    }

    public static int GetOutsideTemp() {
        return OutsideTemp;

    }
}
